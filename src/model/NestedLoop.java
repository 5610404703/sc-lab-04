package model;

public class NestedLoop {
	private int size;
	private int i,j;
	
	public StringBuffer form_1(){		
		StringBuffer listStar1 = new StringBuffer();
		
		for(i=1; i<=size; i++){
			
			for (j=1; j<= size ; j++){
				listStar1.append("*");
			}
			
			listStar1.append("\n");
			
		}	
		return listStar1;
	}
	

	public StringBuffer form_2(){	
		StringBuffer listStar2 = new StringBuffer();
		
		for(i=1; i<=size; i++){
			
			for(j=1; j<=i ;j++){
				listStar2.append("*");
			}
			
			listStar2.append("\n");
		}
		return listStar2;
	}
	
	
	public StringBuffer form_3(){	
		StringBuffer listStar3 = new StringBuffer();
		
		for(i=1; i<=size; i++){
			
			for(j=1; j<=size+(size-1) ;j++){				
				if (j%2 == 0){
					listStar3.append("*");
				}				
				else{
					listStar3.append("-");
				}				
			}
			
			listStar3.append("\n");
			
		}
		return listStar3;
	}
	
	
	public StringBuffer form_4(){	
		StringBuffer listStar4 = new StringBuffer();
		
		for(i=1; i<=size; i++){
			
			for(j=1; j<=size+(size-1) ;j++){				
				if ((i+j)%2 == 0){
					listStar4.append("*");
				}			
				else{
					listStar4.append(" ");
				}				
			}
			
			listStar4.append("\n");
			
		}	
		return listStar4;
	}
	

	public int getSize() {
		return size;
	}


	public void setSize(int size) {
		this.size = size;
	}

}
