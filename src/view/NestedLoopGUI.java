package view;

import java.awt.BorderLayout;
import java.awt.TextArea;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import model.NestedLoop;


public class NestedLoopGUI {
	
	private JFrame frame;
	private JPanel panelUp, panelDown, upLeft, upRight, downLeft, downRight;
	private TextArea boxInput;
	private TextArea boxResult;
	private JComboBox<String> comboBoxFormResult;
	private JButton runButton;
	private NestedLoop nestedLoop;
	
	public NestedLoopGUI() {
		createPanel();
		createPanel();
		createTextAreaInput();
		createTextAreaResult();
		createCombobox();
		creatButton();
		createFrame(); 
   }
	
	public void createPanel(){
		panelUp = new JPanel();
		panelUp.setLayout(new BorderLayout());
		panelDown = new JPanel();
		panelDown.setLayout(new BorderLayout());
		upLeft = new JPanel();
		upLeft.setBorder(new TitledBorder(UIManager.getBorder("TitleBorder.border"), "Input size:", TitledBorder.LEADING, TitledBorder.TOP));
		upRight = new JPanel();
		upRight.setBorder(new TitledBorder(UIManager.getBorder("TitleBorder.border"), "Result:", TitledBorder.LEADING, TitledBorder.TOP));
		downLeft = new JPanel();
		downRight = new JPanel();
	}
	
	
	public void createTextAreaInput(){
		boxInput = new TextArea();
	}
	
	
	public void createTextAreaResult(){
		boxResult = new TextArea();
	}
	
	public void createCombobox(){
		comboBoxFormResult = new JComboBox<String>();
		comboBoxFormResult.addItem("form 1");
		comboBoxFormResult.addItem("form 2");
		comboBoxFormResult.addItem("form 3");
		comboBoxFormResult.addItem("form 4");
	}
	
	
	public void creatButton(){
		runButton = new JButton("Submit");
	}

	
   public void setListener(ActionListener list) {
		runButton.addActionListener(list);
   }
   
   
   public void createFrame() {
		frame = new JFrame();
		frame.setLayout(new BorderLayout());
		frame.add(panelUp, BorderLayout.NORTH);
		frame.add(panelDown, BorderLayout.CENTER);
		
		panelUp.add(upLeft, BorderLayout.WEST);
		panelUp.add(upRight,BorderLayout.EAST);
		
		panelDown.add(downLeft,BorderLayout.WEST); 
		panelDown.add(downRight,BorderLayout.EAST); 
		
		upLeft.add(boxInput, BorderLayout.CENTER);
		upRight.add(boxResult, BorderLayout.CENTER);
		
		downLeft.add(comboBoxFormResult);		
		downRight.add(runButton);
		
		boxResult.setEditable(false);
		
		nestedLoop = new NestedLoop();
		

		
		runButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				System.out.println(boxInput.getText().toString());
				
				int n = Integer.parseInt(boxInput.getText());
				
				nestedLoop.setSize(n);	
				String form = comboBoxFormResult.getSelectedItem().toString();
				
				if (form == "form 1"){
					StringBuffer listStar = nestedLoop.form_1();
					boxResult.setText(listStar.toString());
				}	
				
				else if (form == "form 2"){
					StringBuffer listStar = nestedLoop.form_2();
					boxResult.setText(listStar.toString());
				}
				
				else if (form == "form 3"){
					StringBuffer listStar = nestedLoop.form_3();
					boxResult.setText(listStar.toString());
				}
				
				else if (form == "form 4"){
					StringBuffer listStar = nestedLoop.form_4();
					boxResult.setText(listStar.toString());
				}

			}		
		});
		
		 
		frame.pack();
		frame.setLayout(null);
		frame.setSize(927,300);
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
   }


   public TextArea getInputField() {
	   return boxInput;
   }
 


 
}
